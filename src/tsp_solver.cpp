// Copyright 2023 jncfa
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#include <algorithm>
#include <cmath>
#include <iostream>
#include <limits>
#include <numeric>
#include <stdexcept>
#include <string>
#include <tuple>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "rclcpp/rclcpp.hpp"

#include "jncfa_knapp_ros2pkg/fs_utils.hpp"
#include "jncfa_knapp_ros2pkg/general_utils.hpp"
#include "jncfa_knapp_ros2pkg/tsp_solver.hpp"

auto tsp_logger = rclcpp::get_logger("tsp_solver");

// helper struct that holds the current cost and the ordered list of the nodes
// visited for finding the optimal solution
struct helper_struct
{
  double cost;
  std::vector<uint64_t> visited_nodes;

  helper_struct() {}
  helper_struct(const double & cost, const std::vector<uint64_t> & visited_nodes)
  : cost(cost), visited_nodes(visited_nodes) {}
};

std::vector<std::vector<double>> build_adjacency_mtx(
  const std::vector<geometry_msgs::msg::Point> & positions)
{
  // init matrix with the correct size
  std::vector<std::vector<double>> adjancency_mtx(
    positions.size(), std::vector<double>(positions.size(), 0));

  // loop only through matrix above the diagonal, the rest of the values are
  // symmetrical
  for (size_t idx_x = 0; idx_x < positions.size(); idx_x++) {
    for (size_t idx_y = 0; idx_y < idx_x; idx_y++) {
      const auto & p1 = positions[idx_x], & p2 = positions[idx_y];
      double dist =
        std::sqrt(std::pow(p1.x - p2.x, 2) + std::pow(p1.y - p2.y, 2));

      // add distances to the mtx
      adjancency_mtx[idx_y][idx_x] = dist;
      adjancency_mtx[idx_x][idx_y] = dist;
    }
  }
  return adjancency_mtx;
}

std::vector<std::vector<double>> build_adjacency_mtx(
  const std::vector<std::tuple<std::string, geometry_msgs::msg::Point>> &
  positions)
{
  // init matrix with the correct size
  std::vector<std::vector<double>> adjancency_mtx(
    positions.size(), std::vector<double>(positions.size(), 0));

  // loop only through matrix above the diagonal, the rest of the values are
  // symmetrical
  for (size_t idx_x = 0; idx_x < positions.size(); idx_x++) {
    for (size_t idx_y = 0; idx_y < idx_x; idx_y++) {
      const auto & p1 = std::get<1>(positions[idx_x]),
      & p2 = std::get<1>(positions[idx_y]);
      double dist =
        std::sqrt(std::pow(p1.x - p2.x, 2) + std::pow(p1.y - p2.y, 2));

      // add distances to the mtx
      adjancency_mtx[idx_y][idx_x] = dist;
      adjancency_mtx[idx_x][idx_y] = dist;
    }
  }
  return adjancency_mtx;
}

std::vector<uint64_t> dp_tsp_solver(
  const std::vector<std::vector<double>> & adjacency_mtx)
{
  if (adjacency_mtx.size() < 2) {
    throw std::invalid_argument("Invalid adjancency matrix");
  } else if (adjacency_mtx.size() == 2) {
    // if there's only 2 nodes, then the first node is the starting node, and
    // the last is the end node
    return std::vector<uint64_t>({});
  } else if (adjacency_mtx.size() == 3) {
    // with 3 nodes, then the first node is the mid node, the second node is the
    // starting node and the last is the end node
    return std::vector<uint64_t>({0});
  }

  // to solve the TSP problem with DP we're tabulating all the subproblems, and
  // interatively end up solving the global problem g(n, S) = min_{c in
  // S}(M_{nc} + g(c, S - n), where g(n, S) means the minimal cost to travel to n,
  // given the set of S visited nodes. for tabulating purposes, S does not
  // contain the start node

  // the algo works like this:
  // start by calculating g(n, 0), i.e., calculate the travel distance from the
  // start node to every node (except the end node) then g(n, S), where S has
  // only 1 element, 2, 3... until we perform this with S being equal to (number
  // of nodes - 1) and for every node (except the start and end node) after
  // that, loop again through the nodes to find the distance from each node to
  // the end_node, i.e get g(end_node, S), where S is every node (except start
  // and end nodes)

  // we can describe the table as a vector of unordered maps, where each map has
  // the best solution to the subproblem for the given set of visited nodes set,
  // which is described by a bitmask of the visited nodes indices the visited
  // nodes list does not contain the start and end nodes, since they'll always
  // be visited first and last respectively

  // get number of nodes besides the start and end node
  const auto number_of_nodes = adjacency_mtx.size() - 2;

  // get indices of start and end node positions on the adj matrix for
  // convenience
  const auto start_node_idx = adjacency_mtx.size() - 2;
  const auto end_node_idx = adjacency_mtx.size() - 1;

  // initialize table and reserve memory to prevent rehashing
  // each unordered_map will have a total size of 2**(n-1) by the end
  std::vector<std::unordered_map<std::string, helper_struct>> tabulated_cost(
    number_of_nodes, std::unordered_map<std::string, helper_struct>(
      std::pow(2, number_of_nodes - 1)));

  // create an empty mask (which represents the first subproblem to solve, no
  // visited nodes)
  std::string empty_mask(number_of_nodes, 0);
  for (size_t node_idx = 0; node_idx < number_of_nodes; node_idx++) {
    RCLCPP_DEBUG(
      tsp_logger, "testing %ld->%ld, travelled %f", start_node_idx, node_idx,
      adjacency_mtx[start_node_idx][node_idx]);
    std::vector<size_t> temp;
    temp.reserve(number_of_nodes);
    tabulated_cost[node_idx][empty_mask] =
      helper_struct(adjacency_mtx[start_node_idx][node_idx], temp);
  }

  // loop for the number of elements in set S
  for (size_t size_of_s = 1; size_of_s < number_of_nodes; size_of_s++) {
    RCLCPP_DEBUG(tsp_logger, "solving subproblems for len(S) = %ld \n", size_of_s);
    //  current node from which we get g(n, S)
    for (size_t current_node_idx = 0; current_node_idx < number_of_nodes;
      current_node_idx++)
    {
      RCLCPP_DEBUG(tsp_logger, "subproblems ending at node %ld", current_node_idx);
      //  build all possible combinations of set S if we want to go to node
      //  [current_node_idx]

      // the combination algorithm I used here is based on
      // https://rosettacode.org/wiki/Combinations#C.2B.2B the idea is using a
      // bitmask that represents the set of visited nodes, and then permuting
      // over it since it's a bitmask, we don't have information about the order
      // of which nodes were visited first, so permuting over this will provide
      // us with all possible combinations. std::next_permutations provides
      // permutations lexicografically ordered, so each combination is
      // guaranteed to only show up once

      std::string visited_nodes_bitmask(size_of_s, 1);   // K leading 1's
      visited_nodes_bitmask.resize(number_of_nodes, 0);  // N-K trailing 0's

      // run through all possible combinations
      do {
        // skip current iteration if visited nodes bitmask flags the current
        // node index, since it's not a valid case
        if (visited_nodes_bitmask[current_node_idx]) {
          continue;
        }

        // store lowest cost
        auto lowest_cost = std::numeric_limits<double>::max();
        auto lowest_cost_next_node_idx = std::numeric_limits<size_t>::max();
        std::vector<size_t> lowest_cost_visted_nodes;

        // loop through all possible indices
        for (size_t idx = 0; idx < number_of_nodes; idx++) {
          if (visited_nodes_bitmask[idx]) {
            // create a copy of the bitmask and mark this node as unvisited (i.e
            // set to 0)
            std::string copied_mask = visited_nodes_bitmask;
            copied_mask[idx] = 0;

            // calculate distance from current node to the node flagged on the
            // bitmask + subproblem g(new_node, S - new_node)
            auto current_cost = tabulated_cost[idx][copied_mask].cost +
              adjacency_mtx[idx][current_node_idx];

            RCLCPP_DEBUG(
              tsp_logger, "node %i -> %i, current cost %f, previous cost %f", idx,
              current_node_idx, current_cost, lowest_cost);
            // update the values if we found a better result
            if (current_cost < lowest_cost) {
              lowest_cost = current_cost;
              lowest_cost_visted_nodes =
                tabulated_cost[idx][copied_mask].visited_nodes;
              lowest_cost_next_node_idx = idx;
            }
          }
        }

        // add the best node to the list of visited nodes and add the cost to
        // the table
        lowest_cost_visted_nodes.push_back(lowest_cost_next_node_idx);
        tabulated_cost[current_node_idx][visited_nodes_bitmask] =
          helper_struct(lowest_cost, lowest_cost_visted_nodes);
      } while (std::prev_permutation(
          visited_nodes_bitmask.begin(),
          visited_nodes_bitmask.end()));
    }
  }

  // now we find the lowest cost to the end_node
  auto final_lowest_cost = std::numeric_limits<double>::max();
  auto final_lowest_cost_next_node_idx = std::numeric_limits<size_t>::max();
  std::vector<uint64_t> final_lowest_cost_visited_nodes;

  // get mask with all nodes selected and loop through all nodes again to find
  // the penultimum node
  std::string bitmask_allflags(number_of_nodes, 1);
  for (size_t node_idx = 0; node_idx < number_of_nodes; node_idx++) {
    // copy bitmask and flag this node as unvisited to get the subproblem
    std::string copied_mask = bitmask_allflags;
    copied_mask[node_idx] = 0;
    auto current_cost = tabulated_cost[node_idx][copied_mask].cost +
      adjacency_mtx[node_idx][end_node_idx];

    RCLCPP_DEBUG(
      tsp_logger, "node %i -> %i, current cost %f, previous cost %f", node_idx,
      end_node_idx, current_cost, final_lowest_cost);
    if (current_cost < final_lowest_cost) {
      final_lowest_cost = current_cost;
      final_lowest_cost_next_node_idx = node_idx;
      final_lowest_cost_visited_nodes =
        tabulated_cost[node_idx][copied_mask].visited_nodes;
    }
  }

  // add last node and return list
  final_lowest_cost_visited_nodes.push_back(final_lowest_cost_next_node_idx);
  return final_lowest_cost_visited_nodes;
}

std::vector<uint64_t> bf_tsp_solver(
  const std::vector<std::vector<double>> & adjacency_mtx)
{
  if (adjacency_mtx.size() < 2) {
    throw std::invalid_argument("Invalid adjancency matrix");
  } else if (adjacency_mtx.size() == 2) {
    // if there's only 2 nodes, then the first node is the starting node, and
    // the last is the end node
    return std::vector<size_t>({});
  } else if (adjacency_mtx.size() == 3) {
    // with 3 nodes, then the first node is the mid node, the second node is the
    // starting node and the last is the end node
    return std::vector<size_t>({0});
  }

  // get number of nodes besides the start and end node
  const auto number_of_nodes = adjacency_mtx.size() - 2;

  // get indices of start and end node positions on the adj matrix for
  // convenience
  const auto start_node_idx = adjacency_mtx.size() - 2;
  const auto end_node_idx = adjacency_mtx.size() - 1;

  // fill path to take
  std::vector<uint64_t> visited_nodes(number_of_nodes, 0);
  std::iota(visited_nodes.begin(), visited_nodes.end(), 0);

  // store best result yet
  std::vector<uint64_t> optimal_path;
  auto min_cost = std::numeric_limits<double>::max();

  // permute through all possible visited_nodes combinations
  do {
    // add cost from start node to first node to visit
    double total_cost = adjacency_mtx[start_node_idx][*visited_nodes.cbegin()];

    // add cost for all path taken
    for (auto it_prev = visited_nodes.cbegin(), it_next = it_prev + 1;
      it_next != visited_nodes.cend(); it_next++, it_prev++)
    {
      total_cost += adjacency_mtx[*it_prev][*it_next];
    }

    // add cost from final visited node to end node
    total_cost += adjacency_mtx[*(visited_nodes.cend() - 1)][end_node_idx];

    // update best result
    if (total_cost < min_cost) {
      optimal_path = visited_nodes;
      min_cost = total_cost;
    }
  } while (std::next_permutation(visited_nodes.begin(), visited_nodes.end()));
  return optimal_path;
}
