// Copyright 2023 jncfa
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.


#include <array>
#include <chrono>
#include <cstdint>
#include <functional>
#include <memory>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <tuple>
#include <vector>

#include "geometry_msgs/msg/point.hpp"
#include "geometry_msgs/msg/pose_stamped.hpp"
#include "jncfa_knapp_ros2pkg/config_parser.hpp"
#include "jncfa_knapp_ros2pkg/fs_utils.hpp"
#include "jncfa_knapp_ros2pkg/general_utils.hpp"
#include "jncfa_knapp_ros2pkg/msg/next_order.hpp"
#include "jncfa_knapp_ros2pkg/order_parser.hpp"
#include "jncfa_knapp_ros2pkg/tsp_solver.hpp"
#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"
#include "visualization_msgs/msg/marker_array.hpp"

using std::placeholders::_1;

class OrderOptimizer : public rclcpp::Node
{
public:
  OrderOptimizer()
  : Node("order_optimizer"), order_file_parser()
  {
    // get parameters (just base path for now)
    this->declare_parameter(
      "base_path", "/root/ros2_ws/src/jncfa_knapp_ros2pkg/tests/test_data");

    // read base path and get config data
    this->base_path = this->get_parameter("base_path").as_string();
    config_data = read_config_file(base_path + "/configuration/products.yaml");

    // setup the topic subscribers
    this->next_order_subscriber =
      this->create_subscription<jncfa_knapp_ros2pkg::msg::NextOrder>(
      "nextOrder", 10,
      std::bind(&OrderOptimizer::next_order_handler, this, _1));

    this->current_position_subscriber =
      this->create_subscription<geometry_msgs::msg::PoseStamped>(
      "currentPosition", 10,
      std::bind(&OrderOptimizer::current_position_handler, this, _1));

    // setup publishers
    this->path_publisher =
      this->create_publisher<visualization_msgs::msg::MarkerArray>(
      "order_path", 10);
  }

private:
  void next_order_handler(
    const jncfa_knapp_ros2pkg::msg::NextOrder::SharedPtr next_order_info)
  {
    RCLCPP_INFO(
      this->get_logger(), "Working on order %i (%s)",
      next_order_info->order_id,
      next_order_info->description.c_str());

    // check if we have a valid position
    if (latest_known_pose.header.frame_id.empty()) {
      RCLCPP_ERROR(
        this->get_logger(),
        "Could not find current position of robot, ignoring...");
      return;
    }

    // try to find order, if it's not valid then it was not found
    // auto order_data = find_order_to_fulfill(base_path,
    // next_order_info->order_id);
    auto order_data = this->order_file_parser.find_order_to_fulfill(
      base_path, next_order_info->order_id);
    if (!order_data.is_valid()) {
      RCLCPP_ERROR(
        this->get_logger(),
        "Could not find order %i in the order list, ignoring...",
        next_order_info->order_id);
      return;
    }

    // get all product data for this order (using std transform with a lambda to
    // map product ids to product data)
    std::vector<ProductData> product_data;
    std::transform(
      order_data.product_list.begin(), order_data.product_list.end(),
      std::back_inserter(product_data), [this](const uint64_t & product_id) {
        return this->config_data.product_data_map.at(product_id);
      });

    // use an unordered set to list all possible locations (from the order
    // files, there seems to be at most 3)
    std::unordered_set<std::string> unique_parts(3);

    // loop through all products and insert all data in the set
    for (const auto & product : product_data) {
      RCLCPP_DEBUG(
        this->get_logger(), "Need this products %i, '%s' ",
        product.product_id, product.description.c_str());
      for (const auto & part : product.list_of_parts) {
        unique_parts.insert(part);
      }
    }

    // we know now how many unique parts there are to calculate the adjancency
    // matrix first build a vector with all unique locations
    std::vector<std::tuple<std::string, geometry_msgs::msg::Point>>
    locations_to_visit;

    for (const auto & part : unique_parts) {
      RCLCPP_DEBUG(this->get_logger(), "Need this part %s", part.c_str());
      locations_to_visit.push_back(
        {part, this->config_data.part_location_map.at(part).pt});
    }

    // add start position
    locations_to_visit.push_back(
      {"order_start", this->latest_known_pose.pose.position});

    // add end position
    locations_to_visit.push_back({"order_dest", order_data.dest});

    // calculate adjancency matrix and tsp solution
    std::vector<uint64_t> optimal_path_tsp;

    // using a wrapper + lambda for measuring the runtime of the algo (wrapper
    // is just for debugging)
    auto time_tsp = timeit_us(
      [&optimal_path_tsp, locations_to_visit]() {
        optimal_path_tsp = bf_tsp_solver(build_adjacency_mtx(locations_to_visit));
      });

    // dp is better for N > 8
    // auto time_tsp = timeit_us([&optimal_path_tsp, locations_to_visit](){
    //  optimal_path_tsp =
    //  dp_tsp_solver(build_adjacency_mtx(locations_to_visit));
    //});

    RCLCPP_DEBUG(
      this->get_logger(),
      "tspsolver.dp (took %lu us) got this path: %s", time_tsp,
      vectorToCommaSeparatedString(optimal_path_tsp).c_str());

    // Draw path in between points

    // Create the marker list
    visualization_msgs::msg::MarkerArray marker_array;

    // Base for the other markers
    visualization_msgs::msg::Marker starting_marker;
    starting_marker.header.frame_id =
      this->latest_known_pose.header.frame_id;    // Set the frame ID
    starting_marker.header.stamp = this->now();   // Get current time
    starting_marker.ns = "OrderOptimizerPath";    // Set the namespace
    starting_marker.action =
      visualization_msgs::msg::Marker::ADD;    // Set the action to add the
                                               // marker
    starting_marker.scale.set__x(100).set__y(100).set__z(100);  // Set the scale
    starting_marker.color.r = 1.0;  // Set the color to red
    starting_marker.color.g = 0.0;
    starting_marker.color.b = 0.0;
    starting_marker.color.a = 1.0;
    starting_marker.lifetime = rclcpp::Duration(10, 0);  // Set the lifetime

    int32_t current_id = 0;
    // loop through all nodes
    for (const auto & node_idx : optimal_path_tsp) {
      auto next_node = locations_to_visit[node_idx];

      // get part name
      auto part_name = std::get<0>(next_node);

      // get next position
      auto next_position = std::get<1>(next_node);

      auto copied_marker = starting_marker;
      copied_marker.id = current_id;
      copied_marker.type = visualization_msgs::msg::Marker::
        CYLINDER;                  // Set the marker type to a cylider as its a part location
      copied_marker.pose.position = next_position;  // Set the position
      // copied_marker.pose.orientation.x = 0; // Disregarding the orientation
      // for now

      marker_array.markers.push_back(copied_marker);

      // since there may be multiple pickups at the same location, we will use a
      // single marker to represent all of them but post log messages for each
      // individual pick up
      for (const auto & product : product_data) {
        for (const auto & part : product.list_of_parts) {
          if (part == part_name) {
            RCLCPP_INFO(
              this->get_logger(),
              "%i. Fetching part ‘%s’ for product ‘%i’ at x: %f, y: %f",
              ++current_id, part_name.c_str(), product.product_id,
              next_position.x, next_position.y);
          }
        }
      }
    }

    RCLCPP_INFO(
      this->get_logger(),
      "%i. Delivering to destination x: %f, y: %f", ++current_id,
      order_data.dest.x, order_data.dest.y);

    auto final_marker = starting_marker;
    final_marker.id = current_id;
    final_marker.type =
      visualization_msgs::msg::Marker::CUBE;    // Set the marker type to a cube
                                                // as its a destination
    final_marker.pose.position = order_data.dest;
    // final_marker.pose.orientation.x = 1.0;  // Disregarding the orientation
    // for now

    marker_array.markers.push_back(final_marker);
    this->path_publisher->publish(marker_array);
  }

  void current_position_handler(
    const geometry_msgs::msg::PoseStamped::SharedPtr current_position)
  {
    // update last known pose
    this->latest_known_pose.header = current_position->header;
    this->latest_known_pose.pose = current_position->pose;

    RCLCPP_INFO(
      this->get_logger(),
      "Got a new position for the robot: (%f, %f)",
      this->latest_known_pose.pose.position.x,
      this->latest_known_pose.pose.position.y);
  }

  geometry_msgs::msg::PoseStamped latest_known_pose;
  rclcpp::Subscription<jncfa_knapp_ros2pkg::msg::NextOrder>::SharedPtr
    next_order_subscriber;
  rclcpp::Subscription<geometry_msgs::msg::PoseStamped>::SharedPtr
    current_position_subscriber;
  rclcpp::Publisher<visualization_msgs::msg::MarkerArray>::SharedPtr
    path_publisher;

  std::string base_path;
  ConfigData config_data;

  MultiThreadedOrderParser order_file_parser;
};

int main(int argc, char * argv[])
{
  rclcpp::init(argc, argv);
  // rclcpp::spin(std::make_shared<OrderOptimizer>());

  // use static single threaded executor for a slight runtime improvement
  rclcpp::executors::StaticSingleThreadedExecutor executor;

  auto order_optimizer_node = std::make_shared<OrderOptimizer>();
  executor.add_node(order_optimizer_node);
  executor.spin();
  executor.remove_node(order_optimizer_node);

  rclcpp::shutdown();
  return 0;
}
