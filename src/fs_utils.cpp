// Copyright 2023 jncfa
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.


#include <dirent.h>

#include <algorithm>
#include <stdexcept>
#include <string>
#include <vector>

#include "rclcpp/rclcpp.hpp"

#include "jncfa_knapp_ros2pkg/fs_utils.hpp"


std::vector<std::string> list_all_subfolders(const std::string & base_path, const bool & sorted)
{
  // attempt to get directory stream
  DIR * directory = opendir(base_path.c_str());
  if (directory == nullptr) {
    throw std::invalid_argument("Failed to open directory");
  }

  // list all directory entries and put them in subfolder_names
  std::vector<std::string> subfolder_names;
  dirent * entry;

  while ((entry = readdir(directory)) != nullptr) {
    if (entry->d_type == DT_DIR) {  // Check if it's a directory
      subfolder_names.push_back(base_path + entry->d_name);
    }
  }

  // close directory
  closedir(directory);

  // sort if needed
  if (sorted) {
    std::sort(subfolder_names.begin(), subfolder_names.end());
  }
  return subfolder_names;
}

std::vector<std::string> list_all_files(const std::string & base_path, const bool & sorted)
{
  // attempt to get directory stream
  DIR * directory = opendir(base_path.c_str());
  if (directory == nullptr) {
    throw std::invalid_argument("Failed to open directory");
  }

  // list all directory entries and put them in subfolder_names
  std::vector<std::string> subfiles_names;
  dirent * entry;

  while ((entry = readdir(directory)) != nullptr) {
    if (entry->d_type == DT_REG) {  // Check if it's a regular file
      subfiles_names.push_back(base_path + entry->d_name);
    }
  }

  // close directory
  closedir(directory);

  // sort if needed
  if (sorted) {
    std::sort(subfiles_names.begin(), subfiles_names.end());
  }

  return subfiles_names;
}
