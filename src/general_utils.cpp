// Copyright 2023 jncfa
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.


#include "jncfa_knapp_ros2pkg/general_utils.hpp"

#include <sstream>
#include <string>
#include <vector>

#include "rclcpp/rclcpp.hpp"

std::string matrixToCommaSeparatedString(
  const std::vector<std::vector<double>> & matrix)
{
  std::stringstream ss;

  // Iterate over the matrix and append each element to the stringstream
  for (size_t i = 0; i < matrix.size(); ++i) {
    for (size_t j = 0; j < matrix[i].size(); ++j) {
      ss << matrix[i][j];

      // Append a comma if it's not the last element in the row
      if (j != matrix[i].size() - 1) {
        ss << ",";
      }
    }

    // Append a newline character if it's not the last row
    if (i != matrix.size() - 1) {
      ss << "\n";
    }
  }

  // Convert the stringstream to a string
  return ss.str();
}

std::string vectorToCommaSeparatedString(const std::vector<uint64_t> & values)
{
  std::stringstream ss;
  // Iterate over the vector and append each value to the stringstream
  for (size_t i = 0; i < values.size(); ++i) {
    ss << values[i];

    // Append a comma if it's not the last element
    if (i != values.size() - 1) {
      ss << ",";
    }
  }
  // Convert the stringstream to a string
  return ss.str();
}
