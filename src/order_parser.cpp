// Copyright 2023 jncfa
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.


#include "jncfa_knapp_ros2pkg/order_parser.hpp"

#include <algorithm>
#include <functional>
#include <iostream>
#include <stdexcept>
#include <vector>
#include <string>

#include "jncfa_knapp_ros2pkg/fs_utils.hpp"
#include "jncfa_knapp_ros2pkg/general_utils.hpp"
#include "rclcpp/rclcpp.hpp"
#include "yaml-cpp/yaml.h"

rclcpp::Logger order_logger = rclcpp::get_logger("order_parser");

/**
 * Reads a single file until it finds order with the given order id, and returns
 * the order information. Returns an invalid order if the order was not found.
 */

OrderToFulfill read_file_for_order(
  const std::string & filename,
  const uint32_t & wanted_order_id)
{
  RCLCPP_DEBUG(order_logger, "Reading order file: %s", filename.c_str());
  auto config_yaml_rootnode = YAML::LoadFile(filename);

  if (!config_yaml_rootnode.IsSequence()) {  // Check if the YAML is valid
    throw std::invalid_argument(
            "Filename does not point to a valid configuration file");
  }

  RCLCPP_DEBUG(
    order_logger, "Found %i orders in order file",
    config_yaml_rootnode.size());

  // loop through the sequence of YAML nodes
  for (auto it = config_yaml_rootnode.begin(); it != config_yaml_rootnode.end();
    ++it)
  {
    // check if it's a map object
    if (!it->IsMap()) {
      throw std::invalid_argument(
              "Filename does not point to a valid configuration file");
    }

    // check if the map contains all necessary keys
    if (!(*it)["order"] || !(*it)["cx"] || !(*it)["cy"] || !(*it)["products"]) {
      throw std::invalid_argument(
              "Filename does not point to a valid configuration file");
    }

    // go through all keys
    auto order_id = (*it)["order"].as<uint32_t>();

    // but first check if this is the order we are looking for
    if (order_id != wanted_order_id) {
      // it's not, keep lookin'
      continue;
    }

    auto cx = (*it)["cx"].as<double>();
    auto cy = (*it)["cy"].as<double>();

    auto products = (*it)["products"];

    // check if products is a sequence or not
    if (!products.IsSequence()) {
      throw std::invalid_argument(
              "Filename does not point to a valid configuration file");
    }

    // construct product ids vector by converting the node elements to uint32_t
    std::vector<uint32_t> product_ids;
    std::transform(
      products.begin(), products.end(),
      std::back_inserter(product_ids),
      std::bind(&YAML::Node::as<uint32_t>, std::placeholders::_1));

    return OrderToFulfill(order_id, cx, cy, product_ids);
  }

  return OrderToFulfill();
}

/**
 * Reads all of the files in the base path until it finds the order, otherwise
 * returns an invalid order
 */
OrderToFulfill find_order_to_fulfill(
  const std::string & base_path,
  const uint32_t & order_id)
{
  RCLCPP_DEBUG(
    order_logger, "Looking for order %i in %s", order_id,
    base_path.c_str());

  for (const auto & filename : list_all_files(base_path + "/orders/")) {
    auto order_data = read_file_for_order(filename, order_id);

    if (order_data.is_valid()) {
      return order_data;
    }
  }

  // return empty (invalid) order
  return OrderToFulfill();
}

MultiThreadedOrderParser::MultiThreadedOrderParser()
{
  // increase logging verbosity
#ifndef NDEBUG
  if (rcutils_logging_set_logger_level(
      order_logger.get_name(),
      RCUTILS_LOG_SEVERITY_DEBUG))
  {
    throw std::runtime_error("Failed to set logger level");
  }
#endif

  // use half of the threads available, no more than 6
  auto thread_count =
    std::min(std::thread::hardware_concurrency() / 2, (unsigned int)6);
  RCLCPP_DEBUG(
    order_logger, "Creating %i threads for order parsing",
    thread_count);

  // initialize thread pool (set threads as initially working)
  this->num_working_threads = thread_count;
  for (std::vector<std::thread>::size_type thread_idx = 0;
    thread_idx < thread_count; thread_idx++)
  {
    this->thread_pool.push_back(
      std::thread(
        std::bind(
          &MultiThreadedOrderParser::_worker_thread_routine, this, thread_idx)));
  }
}

MultiThreadedOrderParser::~MultiThreadedOrderParser()
{
  RCLCPP_DEBUG(order_logger, "Stopping thread pool...");
  // set flag to indicate termination
  this->should_terminate = true;

  // notify all threads
  this->queue_mut_cond.notify_all();

  // join all threads
  std::for_each(
    this->thread_pool.begin(), this->thread_pool.end(),
    [](std::thread & thread) {
      if (thread.joinable()) {
        thread.join();
      }
    });
}

OrderToFulfill MultiThreadedOrderParser::find_order_to_fulfill(
  const std::string & base_path, const uint32_t & order_id)
{
  RCLCPP_DEBUG(
    order_logger, "Looking for order %i in %s", order_id,
    base_path.c_str());

  // list all files in the folder (to check if files were removed / added)
  auto files_to_read = list_all_files(base_path + "/orders/");

  // reset order, set order_id to find, and list of files to read
  this->order_id_to_find = order_id;
  this->add_files_to_queue(files_to_read);

  // wait until we found something, or processed all data
  auto result = this->wait_for_result();

  // empty queue if it's not empty, and reset order
  this->clear_queue();
  this->order = OrderToFulfill();

  // return result
  return result;
}

void MultiThreadedOrderParser::add_files_to_queue(
  const std::vector<std::string> & files)
{
  RCLCPP_DEBUG(order_logger, "Adding %i files to queue", files.size());

  // get lock and add files to queue
  std::lock_guard<std::mutex> lock(this->queue_mut);
  std::copy(
    files.begin(), files.end(),
    std::back_inserter(this->filename_queue));
}

void MultiThreadedOrderParser::clear_queue()
{
  RCLCPP_DEBUG(order_logger, "Clearing queue...");

  std::lock_guard<std::mutex> lock(this->queue_mut);
  this->filename_queue.clear();
}

OrderToFulfill MultiThreadedOrderParser::wait_for_result()
{
  RCLCPP_DEBUG(order_logger, "Starting all threads...");
  // notify all threads to start working
  this->queue_mut_cond.notify_all();

  // acquire lock to the mutex
  std::unique_lock<std::mutex> lock(this->order_mut);

  RCLCPP_DEBUG(order_logger, "Waiting for result...");

  // wait until we found something or processed all data (workers will notify
  // us)
  this->order_mut_cond.wait(
    lock, [this] {
      return this->order.is_valid() ||
      (this->filename_queue.empty() && this->num_working_threads == 0);
    });

  RCLCPP_DEBUG(order_logger, "No longer waiting!");

  // return order
  return this->order;
}

void MultiThreadedOrderParser::_worker_thread_routine(int thread_id)
{
  while (!this->should_terminate) {
    std::string filename;
    {
      // acquire lock to the mutex
      std::unique_lock<std::mutex> lock(this->queue_mut);

      RCLCPP_DEBUG(order_logger, "Thread %i waiting for work...", thread_id);
      // thread will be locked on queue_mut_cond.wait() until notified
      this->num_working_threads--;

      // if queue is empty, notify threads waiting to prevent deadlock
      if (this->filename_queue.empty()) {
        RCLCPP_DEBUG(
          order_logger,
          "Thread %i didn't see work, queue is empty, notify master..",
          thread_id);
        this->order_mut_cond.notify_all();
      }

      // immediately release it and wait until its notified
      this->queue_mut_cond.wait(
        lock, [this] {
          return !this->filename_queue.empty() || this->should_terminate;
        });

      // thread no longer locked
      this->num_working_threads++;

      // terminate if flagged for it
      if (this->should_terminate) {
        RCLCPP_DEBUG(order_logger, "Thread %i killed!", thread_id);
        return;
      }
      RCLCPP_DEBUG(order_logger, "Thread %i got work!", thread_id);

      // read filename to process order
      filename = this->filename_queue.front();
      this->filename_queue.pop_front();
    }

    RCLCPP_DEBUG(
      order_logger, "Thread %i processing file %s", thread_id,
      filename.c_str());

    // read file for order
    auto result_order = read_file_for_order(filename, order_id_to_find);

    // check if it's valid
    if (result_order.is_valid()) {
      RCLCPP_DEBUG(
        order_logger, "Thread %i found order %i!", thread_id,
        result_order.order_id);

      // indicate that the order was found
      std::lock_guard<std::mutex> lock(this->order_mut);
      this->order = result_order;

      // notify we found order
      this->order_mut_cond.notify_all();
    }
  }
}
