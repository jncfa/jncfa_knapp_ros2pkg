// Copyright 2023 jncfa
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.


#include "jncfa_knapp_ros2pkg/config_parser.hpp"

#include <algorithm>
#include <stdexcept>
#include <vector>
#include <string>

#include "rclcpp/rclcpp.hpp"
#include "yaml-cpp/yaml.h"

rclcpp::Logger logger = rclcpp::get_logger("config_parser");

ConfigData read_config_file(const std::string & filename)
{
  RCLCPP_DEBUG(logger, "Reading config file: %s", filename.c_str());

  auto config_yaml_rootnode = YAML::LoadFile(filename);

  if (!config_yaml_rootnode.IsSequence()) {  // Check if the YAML is valid
    throw std::invalid_argument("Filename does not point to a valid configuration file");
  }

  RCLCPP_DEBUG(logger, "Found %i products in config file", config_yaml_rootnode.size());

  // instantiate the new config
  ConfigData config_data;

  // loop through the sequence of YAML nodes
  for (auto it = config_yaml_rootnode.begin(); it != config_yaml_rootnode.end(); ++it) {
    // check if it's a map object
    if (!it->IsMap()) {
      throw std::invalid_argument("Filename does not point to a valid configuration file");
    }

    // check if the map contains all necessary keys
    if (!(*it)["id"] || !(*it)["product"] || !(*it)["parts"]) {
      throw std::invalid_argument("Filename does not point to a valid configuration file");
    }

    // go through all
    auto product_id = (*it)["id"].as<uint32_t>();
    auto description = (*it)["product"].as<std::string>();
    auto parts = (*it)["parts"];

    // check if parts is a sequence of parts or not
    if (!parts.IsSequence()) {
      throw std::invalid_argument("Filename does not point to a valid configuration file");
    }

    // get part names
    std::vector<std::string> part_names;

    for (auto it_part = parts.begin(); it_part != parts.end(); ++it_part) {
      // check if it's a map object and that it contains the needed keys
      if (!it_part->IsMap()) {
        throw std::invalid_argument("Filename does not point to a valid configuration file");
      }

      if (!(*it_part)["part"] || !(*it_part)["cx"] || !(*it_part)["cy"]) {
        throw std::invalid_argument("Filename does not point to a valid configuration file");
      }

      // add new part to the part_location_map, if none exists, and to the part
      // name collection
      auto part_name = (*it_part)["part"].as<std::string>();
      part_names.push_back(part_name);
      if (config_data.part_location_map.find(part_name) == config_data.part_location_map.end()) {
        config_data.part_location_map[part_name] = PartLocation(
          part_name,
          (*it_part)["cx"].as<double>(), (*it_part)["cy"].as<double>());
      }
    }
    config_data.product_data_map[product_id] =
      ProductData(product_id, description, part_names);
  }
  return config_data;
}
