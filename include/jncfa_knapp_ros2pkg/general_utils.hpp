// Copyright 2023 jncfa
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#ifndef JNCFA_KNAPP_ROS2PKG__GENERAL_UTILS_HPP_
#define JNCFA_KNAPP_ROS2PKG__GENERAL_UTILS_HPP_

#include <chrono>
#include <cstdint>
#include <string>
#include <vector>

/**
 * Function wrapper to measure executed time in miliseconds
 */
template<typename F>
uint64_t timeit_us(F && func)
{
  // Get the current time before executing the function using a monotonic clock
  auto start = std::chrono::steady_clock::now();

  // Execute the function
  func();

  // Get the current time after executing the function
  auto end = std::chrono::steady_clock::now();

  // Calculate the duration in microseconds
  auto duration =
    std::chrono::duration_cast<std::chrono::microseconds>(end - start);

  // Return the execution time in microseconds as a double value
  return duration.count();
}

std::string matrixToCommaSeparatedString(
  const std::vector<std::vector<double>> & matrix);

std::string vectorToCommaSeparatedString(const std::vector<uint64_t> & values);
#endif  // JNCFA_KNAPP_ROS2PKG__GENERAL_UTILS_HPP_
