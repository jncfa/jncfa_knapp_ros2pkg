// Copyright 2023 jncfa
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#ifndef JNCFA_KNAPP_ROS2PKG__CONFIG_PARSER_HPP_
#define JNCFA_KNAPP_ROS2PKG__CONFIG_PARSER_HPP_

#include <cstdint>
#include <unordered_map>
#include <vector>
#include <string>

#include "geometry_msgs/msg/point.hpp"

/**
 * Struct that contains the data of a given product.
 */
struct ProductData
{
  uint32_t product_id;
  std::string description;
  std::vector<std::string> list_of_parts;

  ProductData() {}
  ProductData(
    const uint32_t & product_id, const std::string & description,
    const std::vector<std::string> & list_of_parts)
  : product_id(product_id),
    description(description),
    list_of_parts(list_of_parts) {}
};

/**
 * Struct that contains the location for a given part.
 */
struct PartLocation
{
  std::string part_name;
  geometry_msgs::msg::Point pt;

  PartLocation() {}
  PartLocation(const std::string & part_name, const double & cx, const double & cy)
  : part_name(part_name)
  {
    pt.x = cx;
    pt.y = cy;
  }

  PartLocation(const std::string & part_name, const geometry_msgs::msg::Point & pt)
  : part_name(part_name), pt(pt) {}
};

/**
 * Struct that contains the configuration data for the order optimizer, a
 * hashmap of the part locations (by part name), and a hashmap of the product
 * data (by product_id).
 */
struct ConfigData
{
  // Hashmap of part locations by part name.
  std::unordered_map<std::string, PartLocation> part_location_map;

  // TODO(jncfa): this could be changed into a vector container for faster performance
  // if there's a guarantee that part_id is an integer, and the parts don't have
  // randomly assigned ids
  std::unordered_map<uint32_t, ProductData> product_data_map;

  ConfigData() {}
  ConfigData(
    const std::unordered_map<std::string, PartLocation> & part_location_map,
    const std::unordered_map<uint32_t, ProductData> & product_data_map)
  : part_location_map(part_location_map), product_data_map(product_data_map) {}
};

/**
 * Read file and provide the map with all locations
 */
ConfigData read_config_file(const std::string & filename);

#endif  // JNCFA_KNAPP_ROS2PKG__CONFIG_PARSER_HPP_
