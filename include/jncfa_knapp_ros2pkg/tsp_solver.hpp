// Copyright 2023 jncfa
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#ifndef JNCFA_KNAPP_ROS2PKG__TSP_SOLVER_HPP_
#define JNCFA_KNAPP_ROS2PKG__TSP_SOLVER_HPP_

#include <cstdint>
#include <tuple>
#include <vector>
#include <string>

#include "geometry_msgs/msg/point.hpp"

/**
 * Build adjancency matrix for a given list of positions.
 */
std::vector<std::vector<double>> build_adjacency_mtx(
  const std::vector<geometry_msgs::msg::Point> & positions);

/**
 * Build adjancency matrix for a given list of positions.
 */
std::vector<std::vector<double>> build_adjacency_mtx(
  const std::vector<std::tuple<std::string, geometry_msgs::msg::Point>> &
  positions);

/**
 * Give a optimal solution to the modified travelling salesman problem  (fixed
 * start and end points) using the Held–Karp algorithm. The solver stores all
 * solutions to each subproblem g(n, S) = min_{for c in S}(M[n, c] + g(c, S
 * without c)), in a hash table, where n is the current node, and S is the list
 * of remaining nodes to visit, reducing the computational complexity significantly
 * compared to a brute force solution (from factorial to exponencial growth).
 * The last two nodes of the adjacency matrix are assumed to be the starting node
 * and the ending node, in this order. Returns the path taken (without the start
 * and finish node). Completixity: Time: O(n) = (n**2)*(2**n), Space: O(n) = n*(2**n)
 */
std::vector<uint64_t> dp_tsp_solver(
  const std::vector<std::vector<double>> & adjacency_mtx);

/**
 * Give a optimal (brute-forced) solution to the modified travelling salesman
 * problem. The last two nodes of the adjacency matrix are assumed to be the
 * starting node and the ending node, in this order. Returns the path taken
 * (without the start and finish node).
 */
std::vector<uint64_t> bf_tsp_solver(
  const std::vector<std::vector<double>> & adjacency_mtx);
#endif  // JNCFA_KNAPP_ROS2PKG__TSP_SOLVER_HPP_
