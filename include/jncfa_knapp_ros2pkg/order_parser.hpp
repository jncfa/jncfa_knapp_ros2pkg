// Copyright 2023 jncfa
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#ifndef JNCFA_KNAPP_ROS2PKG__ORDER_PARSER_HPP_
#define JNCFA_KNAPP_ROS2PKG__ORDER_PARSER_HPP_

#include <algorithm>
#include <cstdint>
#include <limits>
#include <string>
#include <thread>
#include <vector>

// used for syncronization primitives
#include <atomic>
#include <condition_variable>
#include <mutex>
#include <deque>

#include "geometry_msgs/msg/point.hpp"

/**
 * Struct that defines an order for the AMR to fulfill.
 */
struct OrderToFulfill
{
  uint32_t order_id;
  geometry_msgs::msg::Point dest;
  std::vector<uint32_t> product_list;

  OrderToFulfill()
  : order_id(std::numeric_limits<uint32_t>::max()) {}
  OrderToFulfill(
    const uint32_t & order_id, const double & dest_cx,
    const double & dest_cy,
    const std::vector<uint32_t> & product_list)
  : order_id(order_id), product_list(product_list)
  {
    dest.set__x(dest_cx).set__y(dest_cy);
  }
  OrderToFulfill(
    const uint32_t & order_id,
    const geometry_msgs::msg::Point & dest,
    const std::vector<uint32_t> & product_list)
  : order_id(order_id), dest(dest), product_list(product_list) {}

  // Check if the order is valid
  bool is_valid() {return order_id != std::numeric_limits<uint32_t>::max();}
};

/**
 * Reads file until it finds order with the given order id, and returns the
 * order information. Returns an invalid order if the order was not found.
 */
OrderToFulfill read_file_for_order(
  const std::string & filename,
  const uint32_t & order_id);

/**
 * Reads all order files until it finds order with the given order id, and
 * returns the order information. Returns an invalid order if the order was not
 * found.
 */
OrderToFulfill find_order_to_fulfill(
  const std::string & base_path,
  const uint32_t & order_id);

class MultiThreadedOrderParser
{
  // pool of allocated threads
  std::vector<std::thread> thread_pool;

  // count number of working threads (to determine if order was not found)
  std::atomic<uint32_t> num_working_threads;

  // indicates if the threads should terminate
  std::atomic<bool> should_terminate;

  // order id to find
  std::atomic<uint32_t> order_id_to_find;

  // order that was found
  OrderToFulfill order;
  std::mutex order_mut;
  std::condition_variable order_mut_cond;

  // queue with the filenames to process
  std::deque<std::string> filename_queue;
  std::mutex queue_mut;
  std::condition_variable queue_mut_cond;

public:
  /**
   * Initializes the thread pool for parsing the order files.
   */
  MultiThreadedOrderParser();

  ~MultiThreadedOrderParser();

  /**
   * Reads all order files until it finds order with the given order id, and
   * returns the order information. Returns an invalid order if the order was
   * not found.
   */
  OrderToFulfill find_order_to_fulfill(
    const std::string & base_path,
    const uint32_t & order_id);

private:
  /**
   * Add files to the queue to be processed by the worker threads.
   */
  void add_files_to_queue(const std::vector<std::string> & files);

  /**
   * Clear the queue of files to be processed by the worker threads.
   */
  void clear_queue();

  /**
   * Wait for the result to be found by the worker threads.
   */
  OrderToFulfill wait_for_result();

  /**
   * Worker thread routine used by the threads in the pool.
   */
  void _worker_thread_routine(int thread_id);
};

#endif  // JNCFA_KNAPP_ROS2PKG__ORDER_PARSER_HPP_
