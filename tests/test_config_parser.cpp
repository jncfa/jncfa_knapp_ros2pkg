// Copyright 2023 jncfa
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.


#include <gtest/gtest-message.h>
#include <gtest/gtest-test-part.h>

#include <memory>
#include <stdexcept>
#include <string>

#include "gtest/gtest_pred_impl.h"

#include "jncfa_knapp_ros2pkg/config_parser.hpp"

/**
 * Tests a minimal working example example
 */
TEST(config_parser_test, test_minimal_example) {
  // get the current file's path
  std::string testFilePath = __FILE__;

  // extract the directory path
  std::string testDirectory = testFilePath.substr(0, testFilePath.rfind('/'));

  // construct the relative path to the YAML file
  std::string yamlFilePath = testDirectory + "/test_data/configuration/products.yaml";

  auto result = read_config_file(yamlFilePath);

  // check if parts are loaded properly
  ASSERT_EQ(result.part_location_map.at("Part A").pt.x, 791.86304);
  ASSERT_EQ(result.part_location_map.at("Part A").pt.y, 732.23236);
  ASSERT_EQ(result.part_location_map.at("Part B").pt.x, 550.09924);
  ASSERT_EQ(result.part_location_map.at("Part B").pt.y, 655.423);
  ASSERT_EQ(result.part_location_map.at("Part C").pt.x, 281.39413);
  ASSERT_EQ(result.part_location_map.at("Part C").pt.y, 68.39627);

  // check if product is loaded properly
  ASSERT_EQ(result.product_data_map.at(1).product_id, (uint32_t)1);
  ASSERT_EQ(result.product_data_map.at(1).description, "Product 1");
  ASSERT_EQ(result.product_data_map.at(1).list_of_parts.size(), (uint32_t)1);
  ASSERT_EQ(result.product_data_map.at(1).list_of_parts[0], "Part A");
}

/**
 * Test a non functional example, should throw
 */
TEST(config_parser_test, throw_file_not_found) {
  // this tests _that_ the expected exception is thrown
  EXPECT_THROW(
  {
    try {
      read_config_file("this must fail, bad file");
    } catch (const std::runtime_error & e) {
      // and this tests that it has the correct message
      EXPECT_STREQ("bad file", e.what());
      throw;
    }
  },
    std::runtime_error);
}

int main(int argc, char ** argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
