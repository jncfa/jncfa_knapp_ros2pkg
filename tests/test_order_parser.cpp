// Copyright 2023 jncfa
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#include <gtest/gtest.h>

#include <string>
#include <stdexcept>
#include <cstdint>

#include "jncfa_knapp_ros2pkg/order_parser.hpp"

/**
 * Tests a minimal working example example
 */
TEST(order_parser_test, test_minimal_example) {
  // get the current file's path
  std::string testFilePath = __FILE__;

  // extract the directory path
  std::string testDirectory = testFilePath.substr(0, testFilePath.rfind('/'));

  // construct the relative path to the YAML file
  std::string yamlFilePath = testDirectory + "/test_data/orders/orders_20201201.yaml";

  auto result = read_file_for_order(yamlFilePath, 1000001);

  ASSERT_TRUE(result.is_valid());

  // manually check if the order is correct
  EXPECT_EQ(result.order_id, (uint32_t)1000001);
  EXPECT_FLOAT_EQ(result.dest.x, 748.944);
  EXPECT_FLOAT_EQ(result.dest.y, 474.71707);
  EXPECT_EQ(result.product_list.size(), (uint32_t)5);
  EXPECT_EQ(result.product_list[0], (uint32_t)902);
  EXPECT_EQ(result.product_list[1], (uint32_t)293);
  EXPECT_EQ(result.product_list[2], (uint32_t)142);
  EXPECT_EQ(result.product_list[3], (uint32_t)56);
  EXPECT_EQ(result.product_list[4], (uint32_t)894);
}

TEST(order_parser_test, test_minimal_not_working_example) {
  // get the current file's path
  std::string testFilePath = __FILE__;

  // extract the directory path
  std::string testDirectory = testFilePath.substr(0, testFilePath.rfind('/'));

  // construct the relative path to the YAML file
  std::string yamlFilePath = testDirectory + "/test_data/orders/orders_20201201.yaml";

  // this order id does not exist
  auto result = read_file_for_order(yamlFilePath, 0);

  // must be invalid
  ASSERT_FALSE(result.is_valid());
}

/**
 * Test a non functional example, should throw
 */
TEST(order_parser_test, throw_file_not_found) {
  // this tests _that_ the expected exception is thrown
  EXPECT_THROW(
  {
    try {
      auto result = read_file_for_order("this must fail, bad file", 0);
    } catch (const std::runtime_error & e) {
      // and this tests that it has the correct message
      EXPECT_STREQ("bad file", e.what());
      throw;
    }
  },
    std::runtime_error);
}

int main(int argc, char ** argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
