// Copyright 2023 jncfa
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.


#include <gtest/gtest.h>

#include <stdexcept>

#include "jncfa_knapp_ros2pkg/tsp_solver.hpp"

/**
 * Tests a minimal working example example
 */
TEST(tsp_solver_tests, bf_test_minimal_example) {
  auto sol = bf_tsp_solver(
  {
    // 0  1  2  s  f
    {0, 1, 2, 1, 3},
    {1, 0, 1, 2, 2},
    {2, 1, 0, 3, 1},
    {1, 2, 3, 0, 4},
    {3, 2, 1, 4, 0},
  });

  ASSERT_EQ(sol.size(), (size_t)3);
  EXPECT_EQ(sol[0], (size_t)0);
  EXPECT_EQ(sol[1], (size_t)1);
  EXPECT_EQ(sol[2], (size_t)2);
}

TEST(tsp_solver_tests, dp_test_minimal_example) {
  auto sol = dp_tsp_solver(
  {
    // 0  1  2  s  f
    {0, 1, 2, 1, 3},
    {1, 0, 1, 2, 2},
    {2, 1, 0, 3, 1},
    {1, 2, 3, 0, 4},
    {3, 2, 1, 4, 0},
  });

  ASSERT_EQ(sol.size(), (size_t)3);
  EXPECT_EQ(sol[0], (size_t)0);
  EXPECT_EQ(sol[1], (size_t)1);
  EXPECT_EQ(sol[2], (size_t)2);
}

int main(int argc, char ** argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
