# jncfa_knapp_ros2pkg

![pipeline](https://gitlab.com/jncfa/jncfa_knapp_ros2pkg/badges/main/pipeline.svg?ignore_skipped=true) ![coverage](https://gitlab.com/jncfa/jncfa_knapp_ros2pkg/badges/main/coverage.svg?ignore_skipped=true)

> José Faria's solution to the Candidate Evaluation Assingment from KNAPP

### Setting up your environment

This package was developed and tested on Ubuntu 20.04 LTS with ROS2 Foxy Fitzroy, more specifically using the `foxy-desktop` image from Docker Hub. You can pull it with:

```bash
docker pull osrf/ros:foxy-desktop

# or simply run it and let Docker pull it for you
docker run osrf/ros:foxy-desktop
```

### Building & running the package
The package does not require any dependencies beyond those of ROS2 Foxy, so you can directly clone and compile the package with:

```bash
# ...
# assuming SSH authentication was previously setup
# ...

# source ROS2 Foxy
source /opt/ros/foxy/setup.bash
# create a workspace
mkdir -p ~/ros2_ws/src
cd ~/ros2_ws/src
# clone the package
git clone git@gitlab.com:jncfa/jncfa_knapp_ros2pkg.git

# compile and source the package
colcon build --packages-select jncfa_knapp_ros2pkg && source install/local_setup.bash

# run it
ros2 run jncfa_knapp_ros2pkg order_optimizer
```

In another terminal, run this to issue the commands to the order optimizer:
```bash
# source ROS2 Foxy & the package
source install/setup.bash
# publish a position and an order
ros2 topic pub -1 /currentPosition geometry_msgs/msg/PoseStamped "{header: {frame_id: 'map'}, pose: {position: {x: 1.0, y: 2.0}}}" && ros2 topic pub -1 /nextOrder jncfa_knapp_ros2pkg/msg/NextOrder "{order_id: 1000002, description: 'hello world'}"
```
    
